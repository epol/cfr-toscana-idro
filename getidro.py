#!/usr/bin/env python3

"""
@file getidro.py
This file is part of Starfleet Arno monitoring.

Copyright (C) 2020 - Enrico Polesel
Copyright (C) 2020 - Fabio Zoratti

This software is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This software is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this software.  If not, see
<https://www.gnu.org/licenses/>.
"""


from requests import get
from re import compile as recompile
from datetime import datetime, timedelta
from elasticsearch import Elasticsearch
from mmh3 import hash as mmh3hash
from pytz import timezone
from elasticsearch import helpers
from decouple import config
from sentry_sdk import init
from logging import getLogger
import json


logger = getLogger(__file__)


def compute_date(day: str, month: str, hour: str, minute: str) -> datetime:
    day = int(day)
    month = int(month)
    hour = int(hour)
    minute = int(minute)
    tz = timezone('Europe/Rome')
    now = datetime.now(tz=tz)
    date = tz.localize(datetime(now.year, month, day, hour, minute))
    if date > now + timedelta(hours=3):
        # Prevent events in the future on year change, but compensate for daylight saving time
        date.replace(year=date.year-1)
    return date


def get_idro_data():
    r = get('https://www.cfr.toscana.it/monitoraggio/stazioni.php?type=idro')
    r.raise_for_status()
    reg = recompile(
        r'^VALUES\[(?P<row_index>\d+)\] = new Array\((?P<values>.*)\);$')
    datereg = recompile(
        r'^(?P<day>\d+)/(?P<month>\d+) (?P<hour>\d+)\.(?P<minute>\d+)$')
    colnames = [
        'Codice', 'Fiume', 'Stazione', 'Provincia',
        'Zona allerta ID', 'Zona allerta', 'val6',
        'val7', 'Livello attuale', 'Portata attuale', 'Dh1', 'Dh3', 'Dh6',
        'Date', 'Stato', 'val15'
    ]

    data = []
    for rawrow in r.text.split('\n'):
        if rawrow.startswith('VALUES'):
            row = {}
            try:
                res = reg.match(rawrow)
                row['id'] = res.group('row_index')
                values = [v.strip('"') for v in res.group('values').split(',')]
                for i in range(len(colnames)):
                    row[colnames[i]] = values[i]
                dateres = datereg.match(row['Date'])
                row['Date'] = compute_date(
                    dateres.group('day'),
                    dateres.group('month'),
                    dateres.group('hour'),
                    dateres.group('minute')
                )
            except:
                print("Something wrong with row "+rawrow)
            else:
                data.append(row)
    return data


def put_data_on_es(data, host='localhost', port=9200, user=None, password=None, proto='http'):
    if (user is not None) and (password is not None):
        url = f"{proto}://{user}:{password}@{host}:{port}/"
    else:
        url = f"{proto}://{host}:{port}/"
    es = Elasticsearch(url)
    for row in data:
        row['_index'] = "idrotoscana"
        row['Date'] = row['Date'].isoformat()
        row['_id'] = mmh3hash(row['Codice'] + row['Date'])
        row['_op_type'] = 'index'
    helpers.bulk(es, data)


if __name__ == "__main__":
    logger.info("Initializing Sentry")
    init(config('SENTRY_DSN', cast=str))
    logger.info("Starting data collection")
    data = get_idro_data()
    logger.info("Sending data to ES")
    put_data_on_es(
        data,
        host=config('ELASTIC_HOST', default='localhost', cast=str).strip(),
        port=config('ELASTIC_PORT', default=9200, cast=int),
        user=config('ELASTIC_USERNAME', default=None).strip(),
        password=config('ELASTIC_PASSWORD', default=None).strip(),
        proto=config('ELASTIC_PROTOCOL', default='http', cast=str).strip(),
    )
    logger.info("Completed")
