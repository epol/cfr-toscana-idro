FROM python:3.7-buster AS script
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY getidro.py ./

ENTRYPOINT ["python", "getidro.py"]
